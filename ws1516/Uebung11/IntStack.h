#pragma once
class IntStack
{
private:
	int N;
	int* arr;
	int len;
public:
	IntStack(int value);
	~IntStack();
	void push(int value);
	int pop();
	bool isEmpty() const;
};

