#include "IntStack.h"
#include <iostream>

int main()
{
	IntStack i(5);
	i.pop();
	i.push(4);
	i.push(7);
	i.push(6);
	i.push(5);
	i.push(3);
	i.push(2);
	i.push(2);
	for (int j = 0; j < 5; j++) {
		std::cout << i.pop() << std::endl;
	}
	getchar();
	return 0;
}
