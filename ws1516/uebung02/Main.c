#include <stdlib.h>
#include <stdio.h>
int prePostMatch(int *aArray, int cnt);
void sieb(int *aArray, int cnt);
int main()
{
	int *array = malloc(99 * sizeof(int));
	for(int i = 0; i <99; ++i)
	{
		array[i] = i + 2;
	}
	int cnt = 99;
	sieb(array, cnt);
	int arr1[] = { 42, 21, 7, 5, 42, 21, 7 };
	printf("PrePostMatch: %d\n", prePostMatch(arr1, 7));
	int arr2[] = { 42, 21, 7, 5, 5, 21 };
	printf("PrePostMatch: %d\n", prePostMatch(arr2, 6));
	getchar();
	return 0;
}
int prePostMatch(int *aArray, int cnt)
{
	int i = 0;
	int j = cnt / 2;
	int miss = 0;
	int counter = 0;
	while (j < cnt)
	{
		if(aArray[i] == aArray[j])
		{
			i++;
			j++;
			counter++;
		}else
		{
			miss++;
			counter = 0;
			i = 0;
			j = (cnt / 2) + miss;
		}
	}
	return counter;
}
void sieb(int *aArray, int cnt)
{
	int toDel = 2;
	int start = 1;
	while(start < cnt)
	{
		for(int i = start; i < cnt; ++i)
		{
			if(aArray[i] % toDel == 0)
			{
				aArray[i] = 0;
			}
		}
		start++;
		toDel++;
	}
	for(int i = 0; i < cnt; ++i)
	{
		if(aArray[i] != 0)
		{
			printf("%d ", aArray[i]);
		}
	}
	printf("\n");
}