#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int compare(void *a, void*b);
int main()
{
	char* vStr[] = { "Linus", "Lucy", "Schroeder", "Snoopy", "Woodstock", "Charlie Brown", "Sally", "Peppermint Patty", "Marcy", "Franklin"};

	int cnt = sizeof(vStr) / sizeof(vStr[0]);

	qsort(vStr, cnt, sizeof(char*), compare);

	for(int i = 0; i < cnt; ++i)
	{
		printf("%s\n", vStr[i]);
	}
	getchar();

	return 0;
}

int compare(void *a, void*b)
{
	char** first = (char**)a;
	char** second = (char**)b;
	return strcmp( *first,  *second);
}