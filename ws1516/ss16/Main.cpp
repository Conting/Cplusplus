#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include "RBN.h"

int fizzBuzz(unsigned int aNum);
int fizzBuzzRec(unsigned int aNum);
int isAnagram(char* w1, char* w2);
int compare(void *a, void *b);
int main()
{
	fizzBuzz(16);
	printf("\n");
	fizzBuzzRec(16);
	printf("\n");
	printf("Is Anagram: %d\n", isAnagram("palme", "lampe"));


	RBN rb(44444444444444);
	rb.display();

	RBN rb2(333);

	rb.mult(rb2);
	rb.display();
	getchar();
}

int compare(const void *a,const void *b)
{
	char* first = (char*)a;
	char* second = (char*)b;
	if (a < b) return -1;
	if (a == b) return 0;
	else return 1;
}

int fizzBuzz(unsigned int aNum)
{ 
	int a = 1;
	while(a <=aNum)
	{
		if (a % 5== 0 && a % 3==0) {
			printf("%s ,", "Fizz Buzz");
			a++;
		}
		else if (a % 5==0) {
			printf("%s ,", "Fizz");
			a++;
		}
		else if (a % 3==0) {
			printf("%s ,", "Buzz");
			a++;
		}
		else
		{
			printf("%d ,", a);
			a++;
		}
	}
	return 1;
}

int fizzBuzzRec(unsigned int aNum)
{	
	if (aNum == 0) return 1;
	fizzBuzzRec(aNum - 1);
	if (aNum % 5 == 0 && aNum % 3 == 0) printf("%s ,", "Fizz Buzz");
	else if (aNum % 5 == 0) printf("%s ,", "Fizz");
	else if (aNum % 3 == 0) printf("%s ,", "Buzz");
	else printf("%d ,", aNum);
}

int isAnagram(char * w1, char * w2)
{
	qsort(&w1, strlen(w1), sizeof(char*), compare);
	qsort(&w2, strlen(w1), sizeof(char*), compare);
	return strcmp(w1, w2);
}
