#include "RBN.h"
#include <cstdlib>
#include <cstdio>

int RBN::getDigit(int aPos) const
{
	if (aPos >= this->cnt || aPos < 0) return 0;
	else return this->stor[aPos];
}

void RBN::setDigit(int aValue, int aPos) const
{
	if (aPos >= this->cnt || aPos < 0) return;
	else this->stor[aPos] = aValue;
}

RBN::RBN(long long  aValue)
{
	this->cnt = 0;
	int* arr =static_cast<int*>(malloc((this->cnt + 1) * sizeof(int)));
	while(aValue > 0)
	{
		int n = aValue % 10;
		aValue /= 10;
		arr[this->cnt++] = n;
		arr = static_cast<int*>(realloc(arr, (this->cnt + 1) * sizeof(int)));
	}
	this->stor = static_cast<int*>(malloc(this->cnt * sizeof(int)));
	for(auto i = 0; i<this->cnt; ++i)
	{
		this->stor[i] = arr[cnt - i - 1];
	}
	free(arr);
}

RBN RBN::add(const RBN &rhs)
{
	auto cnt = 0;
	auto leftOver = 0;
	auto * arr = static_cast<int*>(malloc((cnt+1)*sizeof(int)));
	auto len1 = this->cnt-1;
	auto len2 = rhs.cnt-1;
	while(len1 >=0 || len2 >=0 )
	{
		auto n = this->getDigit(len1) + rhs.getDigit(len2)+leftOver;
		leftOver = 0;
		if(n > 9)
		{
			arr[cnt++] = n % 10;
			leftOver = n / 10;
			arr = static_cast<int*>(realloc(arr, (cnt + 1) * sizeof(int)));
		}else
		{
			arr[cnt++] = n;
			arr = static_cast<int*>(realloc(arr, (cnt + 1) * sizeof(int)));
 		}
		len1--;
		len2--;
	}
	if(leftOver>0)
	{
		arr[cnt++] = leftOver;
	}
	this->stor = arr;
	for(auto i = 0; i < cnt/2; ++i)
	{
		auto zwsp = this->stor[i];
		this->stor[i] = this->stor[cnt - 1 - i];
		this->stor[cnt - 1 - i] = zwsp;
	}
	this->cnt = cnt;
	return *this;
}

RBN RBN::mult(const RBN & rhs)
{
	printf("\nFirst one: \n");
	this->display();
	printf("\nSecond one: \n");
	rhs.display();

	int mult = 1;
	int len2 = rhs.cnt;
	int forNum = rhs.getDigit(len2-1)-1;
	printf("\nCopy: \n");
	RBN a = *this;
	a.display();
	while (len2 != 0)
	{
		
		while (forNum != 0)
		{
			this->add(a);
			forNum--;
		}

		mult *=10;
		len2--;
		forNum = rhs.getDigit(len2-1)*mult;
	}
	printf("\nEND\n");
	return *this;
}


void RBN::display() const
{
	for(auto i = 0; i < this->cnt; ++i)
	{
		printf("%d", this->stor[i]);
	}
	printf("\n");
}

RBN::~RBN()
{
}
