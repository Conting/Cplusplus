#pragma once
class RBN
{
private:
	int cnt;
	int *stor;
	int getDigit(int aPos) const;
	void setDigit(int aValue, int aPos) const;
public:
	RBN(long long aValue);
	RBN add(const RBN &rhs);
	RBN mult(const RBN &rhs);
	void display() const;
	~RBN();

};

