#include <stdio.h>
#include <stdlib.h>

void print_addr(int x);
int *makeInt();
int main()
{
	int a = 4;
	int vValue = 32;
	printf("%p\n", &vValue);
	print_addr(vValue);
	int* num = makeInt();
	printf("First %d\n", *num);
	printf("Second %d\n", *num);
	printf("Third %d\n", *num);
	/*
	Es werden verschiedene Kopien angezeigt, da print_addr eine Kopie �bergeben wird.
	*/


	getchar();
	return 0;
}
int *makeInt()
{
	int * toReturn = malloc(sizeof(int));
	*toReturn = 21;
	return toReturn;
}

void print_addr(int x)
{
	printf("%p\n", &x);
}