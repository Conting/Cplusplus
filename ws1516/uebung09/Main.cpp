#include "DatArray.h"
#include <stdlib.h>
#include <cstdio>


int main()
{
	DatArray arr;
	arr.append(5);
	arr.display();
	int aArray[] = { 4,2,6,7 };
	DatArray arr2(aArray, 4);
	arr2.display();
	int aArray2[] = { 7,8,9,5,2,3,4 };
	arr2.replaceValues(aArray2, 7);
	arr2.display();
	getchar();
	return 0;
}
