#pragma once
class DatArray
{
private:
	int *storage;
	int len;
public:
	DatArray();
	DatArray(int *aData, int nCtn);
	~DatArray();

	void append(int aValue);
	void replaceValues(int *aData, int aCnt);
	void display();
};

