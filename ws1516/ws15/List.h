#include <cstddef>

class ListElement {
public:
	int value;
	ListElement *next;
	ListElement(int aValue) : value(aValue), next(NULL) {}
};

class List {
private:
	ListElement *first;

public:
	List();
	void AddElement(int aValue);
	void printAllElementsOfList();
	~List();
};

