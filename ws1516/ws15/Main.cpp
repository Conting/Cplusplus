#include<stdio.h>
#include <stdlib.h>
#include<iostream>
#include <cstdarg>
#include "List.h"
using namespace std;

int maxInt(int n, ...);
int checkBrackets(char* str);
int istFroehlich(unsigned int num);
unsigned int minOfThree(unsigned int a, unsigned int b, unsigned int c);
int main()
{
	//Aufgabe 1
	cout << "maxInt: "<<maxInt(4, 33, 14, 16, 17)<<endl;
	//Aufgabe 2
	cout << "checkBrackets: " << checkBrackets("((xyz)(abc))(ws)") << endl;
	//Aufgabe 4
	auto l = new List();
	l->printAllElementsOfList();
	l->AddElement(42);
	l->printAllElementsOfList();
	l->AddElement(21);
	l->printAllElementsOfList();
	l->AddElement(10);
	l->printAllElementsOfList();
	l->~List();
	//Aufgabe 5
	cout << "isFroehlich: " << istFroehlich(42) << endl;
	//Aufgabe 6
	cout << minOfThree(4, 5, 6) << endl;
	cout << minOfThree(42, 5, 6) << endl;
	cout << minOfThree(42, 52, 6) << endl;
	getchar();
	return 0;
}

int maxInt(int n, ...)
{
	va_list zeiger;
	int biggest;

	va_start(zeiger, n);
	biggest = va_arg(zeiger, int);
	for(auto i = 1; i < n; i++)
	{
		auto temp = va_arg(zeiger, int);
		if (temp > biggest) biggest = temp;
	}
	return biggest;
}

int checkBrackets(char * str)
{
	int count = 0;
	int n = strlen(str);
	for(auto i = 0; i<n; ++i)
	{
		if (str[i] == '(') count++;
		else if (str[i] == ')')
		{
			count--;
			if (count < 0)return 0;
		}
	}
	if (count == 0) return 1;
	return 0;
}

int istFroehlich(unsigned int num)
{
	
	while(true)
	{
		auto sum = 0;
		while(num > 0)
		{
			sum += (num % 10)*(num % 10);
			num /= 10;
		}
		if (sum == 4) return 0;
		if (sum == 1) return 1;
		else
		{
			num = sum;
			sum = 0;
		}
	}
}

unsigned int minOfThree(unsigned int a, unsigned int b, unsigned int c)
{
	auto vMin = 0;

	while (a-- && b-- && c--)
		vMin++;

	return vMin;
}