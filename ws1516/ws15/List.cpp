#include "List.h"
#include <cstdio>


List::List()
{
	first = (ListElement*)NULL;
}

void List::AddElement(int aValue)
{
	ListElement *newOne = new ListElement(aValue);
	if (first == NULL)
		first = newOne;
	else
	{
		ListElement *iter = first;
		while (iter->next != NULL)
		{
			iter = iter->next;
		}
		iter->next = newOne;
	}
}

void List::printAllElementsOfList()
{
	ListElement *iter = first;
	printf("<");
	while(iter!= NULL)
	{
		printf("%d ", iter->value);
		iter = iter->next;
	}
	printf(">\n");
}


List::~List()
{
	ListElement *next, *killMe;

	killMe = first;
	while (killMe != NULL)
	{
		next = killMe->next;
		delete killMe;
		killMe = next;
	}
}
