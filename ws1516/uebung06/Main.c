#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int istFroehlich(unsigned int num);
int redGreenArray(int *aArray, int cnt);
int compare(void *a, void *b);
int sumOfNumbers(char *str);
int sumOfDigits(char *str);
int isPalindrom(char *str);
char *mystrdub(char *str);
char *myStrcat(char* dest, char *src);
int myStrlen(char *str);
int main()
{
	int a[] = { 1,1,2,1,3,1,1};
	printf("Is balanced: %d\n", redGreenArray(a,7));
	printf("ist froehlich: %d\n", istFroehlich(49));
	printf("ist froehlich: %d\n", istFroehlich(42));
	printf("SumOfNum: %d\n", sumOfNumbers("as-15g72fdfd2s1"));
	printf("SumOfDigit: %d\n", sumOfDigits("as-15g72fdfd2s1"));
	printf("IsPalindrom: %d\n", isPalindrom("baab"));
	printf("IsPalindrom: %d\n", isPalindrom("baaab"));
	printf("IsPalindrom: %d\n", isPalindrom("asdasdas"));
	char *str = "Bobolino";
	char *str2 = "Bobo";
	printf("MyStrdub: %s\n", mystrdub(str));
	printf("MyStrcat: %s\n", myStrcat(str,str2));
	printf("MyStrlen: %d\n", myStrlen(str2));
	getchar();
	
	return 0;
}
int myStrlen(char *str)
{
	int toReturn = 0;
	int strIterator = 0;
	
	while (str[strIterator] != '\0')
	{
		++strIterator;
		++toReturn;
	}
	return toReturn;
}
char *myStrcat(char* dest, char *src)
{
	int cntDest = strlen(dest);
	int cntSrc = strlen(src);
	int cnt = cntDest + cntSrc;
	char *toReturn = malloc((cnt + 1) * sizeof(char));
	toReturn[cnt] = '\0';
	for(int i = 0; i < cntDest; ++i)
	{
		toReturn[i] = dest[i];
	}
	int toRetIterator = cntDest;
	int scrIterator = 0;
	while(toReturn[toRetIterator] != '\0')
	{
		toReturn[toRetIterator] = src[scrIterator];
		++toRetIterator;
		++scrIterator;
	}
	return toReturn;
}
char *mystrdub(char *str)
{
	int cnt = strlen(str);
	char *toReturn = malloc((cnt + 1)*sizeof(char));
	for(int i = 0; i < cnt; ++i)
	{
		toReturn[i] = str[i];
	}
	toReturn[cnt] = '\0';
	return toReturn;
}

int isPalindrom(char *str)
{
	int cnt = strlen(str);
	for(int i = 0; i<cnt/2; ++i)
	{
		if (str[i] != str[cnt - 1 - i]) return 0;
	}
	return 1;
}

int sumOfDigits(char *str)
{
	int cnt = strlen(str);
	int sum = 0;
	for(int i = 0; i < cnt; ++i)
	{
		if(str[i]>= '0'&&str[i]<= '9')
		{
			sum += str[i] - 48;
		}
	}
	return sum;
}

int sumOfNumbers(char *str)
{
	int cnt = strlen(str);
	int sum = 0;
	int num = 0;
	int mult = 1;
	for (int i = cnt - 1; i >= 0; --i)
	{
		if (str[i] >= '0' && str[i] <= '9') {
			num += mult * (str[i] - 48);
			mult *= 10;
		}
		else {
			if (str[i] == '-') {
				num *= (-1);
			}
			sum += num;
			num = 0;
			mult = 1;
		}
	}
 	return sum;

}

int istFroehlich(unsigned int num)
{
	while(1)
	{
		auto sum = 0;
		while(num > 0)
		{
			sum += (num % 10)*(num % 10);
			num /= 10;
		}
		if (sum == 1) return 1;
		if (sum == 4) return 0;

		num = sum;
		sum = 0;
	}
}

int compare(void *a, void *b)
{
	return (*(int*)a - *(int*)b);
}

int redGreenArray(int *aArray, int cnt)
{
	qsort(aArray, cnt, sizeof(int), compare);
	int sum = 0;
	for (int i = cnt - 1; i >=0; i--) {
		if (sum >= 0) sum -= aArray[i];
		else sum += aArray[i];
	}
	if (sum == 0) return 1;
	else return 0;
}