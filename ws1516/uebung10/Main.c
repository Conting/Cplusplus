#include <stdio.h>
#include <stdlib.h>
typedef struct
{
	int x;
	int y;
}Point;

Point point[] = { {42,21}, {32,12}, {11,12}, {45,1}, {-4,4} };
int doComparePoints(void *lhs, void *rhs);

int main()
{
	int cnt = sizeof(point) / sizeof(point[0]);
	for (int i = 0; i < cnt; ++i)
		printf("%3d | %3d\n", point[i].x, point[i].y);
	qsort(point, cnt, sizeof(Point), doComparePoints);
	for (int i = 0; i < cnt; ++i)
		printf("%3d | %3d\n", point[i].x, point[i].y);

	getchar();
	return 0;

}

int doComparePoints(void *lhs, void *rhs)
{
	Point* first = (Point*)lhs;
	Point* second = (Point*)rhs;

	if (first->x < second->x) return -1;
	if (first->x == second->x) return 0;
	else return 1;
}