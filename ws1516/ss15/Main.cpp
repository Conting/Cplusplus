#include <stdio.h>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <stdarg.h>
#include <string.h>
#include "Date.h"

using namespace std;

typedef struct {
	char * title;
	int year;
} Movie;

int countChar(char* word, ...);
char* toBinary(unsigned int num);
void printout(Movie *ele);
Movie *inArray(Movie *allFilms, int n, char * aTitle);
int doCompare(const void *a, const void *b);
int prePostMatch(int * aArray, int n);

int main() {
	//Aufgabe 1
	cout << countChar("Doctor", 'o', 'r', 'a', '\0') << endl;
	//Aufgabe 2
	cout << toBinary(42) << endl;
	//Aufgabe 3
	Movie all[] = { { "Cars", 2006 },{ "Minions", 2015 },{ "Planes 2",2014 },{ "Wall-E",2008 } };
	Movie *r1;
	r1 = inArray(all, sizeof(all)/sizeof(all[0]), "Wall-E");
	printout(r1);
	//Aufgabe 4
	Date d;
	d.setTheDate(12, 1, 2014);
	Date e;
	e.setTheDate(24, 3, 2013);
	cout << "Is before = "<<  e.isBefore(d) << endl;
	//Aufgabe 5
	int a[] = { 42,21,7,5,5,21 };
	int cnt1 = sizeof(a) / sizeof(int);

	int b[] = { 42,21,7,5,42,21,7 };
	int cnt2 = sizeof(b) / sizeof(int);

	int c[] = { 42,21,3,4,5,6,7,42,21 };
	int cnt3 = sizeof(c) / sizeof(int);
	cout << "lengthOfArray" << cnt1 << endl;
	cout << prePostMatch(a, cnt1) << endl;
	cout << "lengthOfArray" << cnt2 << endl;
	cout << prePostMatch(b, cnt2) << endl;
	cout << "lengthOfArray" << cnt3 << endl;
	cout << prePostMatch(c, cnt3) << endl;
	getchar();
	return 0;
}
//Aufgabe 5
int prePostMatch(int * aArray, int n) {
	int count = 0;
	int miss = 0;
	int i = n / 2;
	int j = 0;
	while (i< n) {
		if (aArray[j] == aArray[i]) {
			count++;
			j++;
			i++;
		}else{
			count = 0;
			miss++;
			i = (n / 2) + miss;
			j = 0;
		}
	}
	return count;
}

//Aufgabe 3

int doCompare(const void *a, const void *b)
{
	const Movie* first = (Movie*)a;
	const Movie* second = (Movie*)b;

	return strcmp(first->title, second->title);
}

Movie *inArray(Movie *allFilms, int n, char * aTitle)
{
	return  (Movie*) bsearch(&aTitle, allFilms, n, sizeof(Movie),doCompare);
}

void printout(Movie *ele) {
	if (ele == NULL)
		printf("Nicht gefunden\n");
	else
		printf("%s wurde gefunden\n", ele->title);
}


//Aufgabe 2 
char* toBinary(unsigned int num) {
	int count = 0;
	char* arr = (char*) malloc((count +1)* sizeof(char));
	while (num > 0) {
		arr[count++] = (char) (num % 2)+48;
		num /= 2;
		arr = (char*) realloc(arr, sizeof(char)*(count + 1));
	}
	char* toRet = (char*)malloc(sizeof(char)*count);
	arr[count] = '\0';
	toRet[count] = '\0';
	for (int i = 0; i < count; ++i) {
		toRet[i] = arr[count - i - 1];
	}
	free(arr);
	return toRet;
}
//Aufgabe 1
int countChar(char* word, ...) {
	int counter = 0;

	va_list zeiger;

	va_start(zeiger, word);

	char a = va_arg(zeiger, char);

	while (a != '\0') {
		for (int i = 0; i < strlen(word); ++i) {
			if (word[i] == a) counter++;
		}
		a = va_arg(zeiger, char);
	}
	va_end(zeiger);
	return counter;

}