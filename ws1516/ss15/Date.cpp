#include "Date.h"

void Date::setTheDate(int day, int month, int year){
	mDay = day;
	mMonth = month;
	mYear = year;
}

bool Date::isBefore(Date & p)
{
	if (mYear < p.mYear) return true;
	if (mMonth < p.mMonth) return true;
	if (mDay < p.mDay) return true;
	return false;
}



Date::Date()
{
}


Date::~Date()
{
}
