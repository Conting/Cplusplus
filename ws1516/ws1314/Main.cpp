﻿#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include "PointOfTime.h"
#include <iostream>
#include "PointOfTime.h"
#include "Event.h"

using namespace std;

//Aufgabe 1
//Lösung Biene Maya

//Aufgabe 2
#define swap(t,x, y) {t temp; t = x; x = y; y = t}

//Aufgabe 3
// cStrPtr[strlen(cStrPtr)+1] = '\0' ist Fehlerhaft, da strlen davor schon um 1 erweitert wurde und so auf nicht allokierten Speicher zugreift
/*
	int len = strlen(cStrPtr):
	cStrPtr = (char *)realloc(cStrPtr, sizeof(char)*(len+2));
	cStrPtr[strlen (len)] = c;
	cStrPtr[strlen(len)+1] = '\0';

*/

//Aufagbe 4
int main()
{
	PointOfTime p;
	p.set(12, 2, 12);
	PointOfTime p2;
	p2.set(11, 4, 11);
	p.display();
	p2.display();
	PointOfTime p3;
	p3.set(12, 2, 12);
	cout << "p less p3?: "<< p.less(p3)<<endl; //return 0;
	cout << "p less p2?: "<<p.less(p2) << endl; //return 0;
	cout << "p2 less p?: "<<p2.less(p) << endl; //return 1;

	//Aufgabe 5
	Event e;
	e.set(11, 11, 11, "BlaBla");
	e.display();
	Event e2(e);
	e2.display();
	e2.set(1, 1, 1, "babo");
	e2.display();
	Event e3;
	e3.set(12, 11, 11, "Bazzza");
	e = e3;
	e.display();


	int i;
	int a;
	int n = 5;
	for (i = 0; n + i; i--) {
		printf("-");
	}
	
	getchar();
	return 0;
}

//Aufgabe 6
/*
a) for(i = 0; i<n; n--)
b) for(i = 0; -i<n; i--)
c) for(i = 0; i+n; i--)
*/




