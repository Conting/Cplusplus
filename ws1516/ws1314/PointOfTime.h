#pragma once
class PointOfTime
{
private:
	int mHour,
		mDay,
		mMonth;
public:
	PointOfTime();
	virtual ~PointOfTime();
	void set(int h, int d, int m);
	virtual void display() const;

	bool less(const PointOfTime &p) const;

	friend bool operator< (const PointOfTime &p1, const PointOfTime &p2);
	friend bool operator== (const PointOfTime &p1, const PointOfTime &p2);
};

