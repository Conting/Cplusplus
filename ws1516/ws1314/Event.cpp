#include "Event.h"
#include <cstdlib>
#include <cstring>
#include <iostream>
#define _CRT_SECURE_NO_WARNINGS 1


Event::Event()
{
	char desc =  (char) (sizeof(char) + 1);
}


Event::~Event()
{
}

Event::Event(const Event &e)
{
	this->desc = (char*)malloc(sizeof(char)*strlen(e.desc));
	strcpy(this->desc, e.desc);
}

Event & Event::operator=(const Event & e)
{	
	this->desc  = (char*) malloc(sizeof(char)*(strlen(e.desc)));
	strcpy(this->desc, e.desc);
	return *this;
}

void Event::set(int h, int d, int m, char * desc)
{
	PointOfTime::set(h, d, m);
	int len = strlen(desc);
	this->desc =(char*) malloc(sizeof(char)*(len));
	strcpy(this->desc, desc);
}

void Event::display()
{
	std::cout << std::endl;
	std::cout << "EVENT" << std::endl;
	PointOfTime::display();
	std::cout << "Description: " << this->desc << std::endl;
}
