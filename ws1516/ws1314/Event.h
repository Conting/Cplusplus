#pragma once
#include "PointOfTime.h"
class Event : public PointOfTime
{
private:
	char * desc;
public:
	Event();
	virtual ~Event();
	Event(const Event &e);

	Event &operator= (const Event &e);

	void set(int h, int d, int m, char* desc);

	virtual void display();
};

