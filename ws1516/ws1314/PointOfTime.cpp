#include "PointOfTime.h"
#include <iostream>

bool operator< (const PointOfTime &p1, const PointOfTime &p2)
{
	if (p1.mMonth < p2.mMonth) return true;
	else if (p1.mMonth > p2.mMonth) return false;
	if (p1.mDay < p2.mDay) return true;
	else if (p1.mDay > p2.mDay) return false;
	if (p1.mHour < p2.mHour) return true;
	else if (p1.mHour > p2.mHour) return false;
	else return false;
}

bool operator== (const PointOfTime &p1, const PointOfTime &p2) {
	if (p1.mMonth == p2.mMonth &&
		p1.mDay == p2.mDay &&
		p1.mHour == p2.mHour)
	{
		return true;
	}
	else return false;
}

PointOfTime::PointOfTime()
{
}


PointOfTime::~PointOfTime()
{
}


void PointOfTime::display() const
{
	std::cout << std::endl;
	std::cout << "Point of Time" << std::endl;
	std::cout << "Hour: " << this->mHour << " Day: " << this->mDay << " Month: " << this->mMonth << std::endl;
}


bool PointOfTime::less(const PointOfTime& p) const
{
	PointOfTime p2;
	p2.set(this->mHour, this->mDay, this->mMonth);
	return p2 < p;
}

void PointOfTime::set(int h, int d, int m)
{

	this->mHour = h;
	this->mDay = d;
	this->mMonth = m;
}



